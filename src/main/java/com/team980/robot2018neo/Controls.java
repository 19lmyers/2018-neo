package com.team980.robot2018neo;

import com.team980.common.controls.DriverInterface;
import com.team980.powerup.OperatorInterface;
import com.team980.robot2018neo.commands.claw.OpenClaw;
import com.team980.robot2018neo.commands.claw.ShutClaw;
import com.team980.robot2018neo.commands.lift.ManualLiftControl;
import com.team980.robot2018neo.commands.lift.MoveLiftToPosition;
import com.team980.robot2018neo.subsystems.ClawSystem;
import com.team980.robot2018neo.subsystems.DriveSystem;
import com.team980.robot2018neo.subsystems.LiftSystem;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.buttons.Trigger;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;

/**
 * Defines the controls ("operator interface") of the robot.
 */
final class Controls implements DriverInterface, OperatorInterface {

    private Joystick driveStick;
    private Joystick driveWheel;
    private XboxController xboxController;
    //private Joystick operatorBox;

    Controls() {
        driveStick = new Joystick(0);
        driveWheel = new Joystick(1);
        xboxController = new XboxController(2);
        //operatorBox = new Joystick(3);

        //bindButtons(null); //TODO do it here? Somewhere else?
    }

    @Deprecated
    void bindButtons(DriveSystem drive, LiftSystem lift, ClawSystem claw) {

        //TODO Under this system, "hold to disable tip protect" isn't intuitively doable?

        //TODO How do I bind buttons while preserving Controls not knowing about Subsystems
        // + Robot not knowing about Joysticks?

        //TODO Do I even need to make these buttons variables?
        // It kind of helps with readability

        var buttonA = new JoystickButton(xboxController, 1); // XboxController.Button is private for some reason??
        buttonA.whenPressed(new MoveLiftToPosition(lift, LiftSystem.LiftPosition.BOTTOM));

        var buttonB = new JoystickButton(xboxController, 2);
        buttonB.whenPressed(new MoveLiftToPosition(lift, LiftSystem.LiftPosition.SWITCH));

        var buttonY = new JoystickButton(xboxController, 4);
        buttonY.whenPressed(new MoveLiftToPosition(lift, LiftSystem.LiftPosition.SCALE));

        var buttonX = new JoystickButton(xboxController, 3);
        buttonX.whenPressed(new MoveLiftToPosition(lift, LiftSystem.LiftPosition.HOLD_CURRENT)); //TODO should these be ordered by robot function or by controller position?

        var rightStick = new JoystickButton(xboxController, 10);
        rightStick.whenPressed(new ManualLiftControl(lift, this));

        new JoystickButton(xboxController, 2).whenPressed(new ShutClaw(claw));

        var leftTrigger = new Trigger() {
            @Override
            public boolean get() {
                return xboxController.getTriggerAxis(GenericHID.Hand.kLeft) > 0.9;
            }
        };
        leftTrigger.whenActive(new OpenClaw(claw));

        var rightTrigger = new Trigger() {
            @Override
            public boolean get() {
                return xboxController.getTriggerAxis(GenericHID.Hand.kRight) > 0.9;
            }
        };
        rightTrigger.whenActive(new ShutClaw(claw));
    }

    @Override
    public double getThrottle() {
        var throttle = -driveStick.getY();

        throttle = limit(throttle);
        throttle = applyDeadband(throttle, 0.1);

        return throttle;
    }

    @Override
    public double getTurn() {
        var turn = driveWheel.getX();

        turn = limit(turn);
        turn = applyDeadband(turn, 0.05);

        return turn;
    }

    @Override
    public double getLiftCommand() {
        var command = -xboxController.getY(GenericHID.Hand.kRight);

        command = limit(command);
        command = applyDeadband(command, 0.1);

        return command;
    }

    /**
     * Limit motor values to the -1.0 to +1.0 range.
     *
     * @see DifferentialDrive#limit(double)
     */
    private double limit(double value) {
        if (value > 1.0) {
            return 1.0;
        }
        if (value < -1.0) {
            return -1.0;
        }
        return value;
    }

    /**
     * Returns 0.0 if the given value is within the specified range around zero. The remaining range
     * between the deadband and 1.0 is scaled from 0.0 to 1.0.
     *
     * @param value    value to clip
     * @param deadband range around zero
     * @see DifferentialDrive#applyDeadband(double, double)
     */
    private double applyDeadband(double value, double deadband) {
        if (Math.abs(value) > deadband) {
            if (value > 0.0) {
                return (value - deadband) / (1.0 - deadband);
            } else {
                return (value + deadband) / (1.0 - deadband);
            }
        } else {
            return 0.0;
        }
    }
}
