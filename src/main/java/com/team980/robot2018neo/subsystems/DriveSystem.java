package com.team980.robot2018neo.subsystems;

import com.team980.common.drive.DifferentialDriveHelper;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.command.Subsystem;

//TODO How would we restructure a given Subsystem to allow PID?
// Should it be configurable?
// How do we account for changing inputs?
// (we can't exactly static access MAX_SPEED without coupling classes that shouldn't be)
public final class DriveSystem extends Subsystem implements DifferentialDriveHelper.DifferentialDrive {

    private static final double SHIFT_UP_POINT = 4.5;
    private static final double SHIFT_DOWN_POINT = 4.0;

    private SpeedController leftDrive;
    private Encoder leftEncoder;

    private SpeedController rightDrive;
    private Encoder rightEncoder;

    private Solenoid shifterSolenoid;

    private boolean isAutoShiftEnabled = false;

    public DriveSystem(SpeedController leftDrive, Encoder leftEncoder, SpeedController rightDrive, Encoder rightEncoder, Solenoid shifterSolenoid) {
        this.leftDrive = leftDrive;
        this.leftEncoder = leftEncoder;

        this.rightDrive = rightDrive;
        this.rightEncoder = rightEncoder;

        this.shifterSolenoid = shifterSolenoid;
    }

    @Override
    protected void initDefaultCommand() {} //Delegated to Robot

    public Gear getGear() {
        return (shifterSolenoid.get() == Gear.LOW.solenoidValue) ? Gear.LOW : Gear.HIGH;
    }

    public void setGear(Gear gear) {
        shifterSolenoid.set(gear.solenoidValue);
    }

    public boolean isAutoShiftEnabled() {
        return isAutoShiftEnabled;
    }

    public void setAutoShiftEnabled(boolean isAutoShiftEnabled) {
        this.isAutoShiftEnabled = isAutoShiftEnabled;
    }

    /**
     * @param left  Requested left command, from -1 to 1
     * @param right Requested right command, from -1 to 1
     */
    @Override
    public void set(double left, double right) { //TODO what do we name this method?
        leftDrive.set(left);
        rightDrive.set(right);

        if (isAutoShiftEnabled) {
            if (Math.abs(leftEncoder.getRate()) > SHIFT_UP_POINT && Math.abs(rightEncoder.getRate()) > SHIFT_UP_POINT) {
                setGear(Gear.HIGH);
            } else if (Math.abs(leftEncoder.getRate()) < SHIFT_DOWN_POINT && Math.abs(rightEncoder.getRate()) < SHIFT_DOWN_POINT) {
                setGear(Gear.LOW);
            }
        }
    }

    public enum Gear {
        LOW(true),
        HIGH(false);

        private boolean solenoidValue;

        Gear(boolean solenoidValue) {
            this.solenoidValue = solenoidValue;
        }
    }
}
