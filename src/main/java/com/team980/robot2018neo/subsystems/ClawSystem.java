package com.team980.robot2018neo.subsystems;

import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;

public class ClawSystem extends Subsystem {

    private Solenoid clawSolenoid;

    public ClawSystem(Solenoid clawSolenoid) {
        this.clawSolenoid = clawSolenoid;
    }

    @Override
    protected void initDefaultCommand() {} //ignored

    public void open() {
        clawSolenoid.set(true);
    }

    //TODO close() was taken :(
    public void shut() {
        clawSolenoid.set(false);
    }
}
