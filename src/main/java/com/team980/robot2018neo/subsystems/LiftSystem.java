package com.team980.robot2018neo.subsystems;

import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 * I'm going to pretend this is the 2018 robot for a test.
 */
public final class LiftSystem extends Subsystem {

    //TODO I'm still undecided on these subsystem-local configuration blocks.

    private static final double LIFT_POSITION_DEADBAND = 3000;

    private static final double MAX_UPWARD_COMMAND = 0.7;
    private static final double MAX_DOWNWARD_COMMAND = 0.7;

    private SpeedController liftController;
    private Encoder liftEncoder;

    private LiftPosition targetPosition;

    public LiftSystem(SpeedController liftController, Encoder liftEncoder) {
        this.liftController = liftController;
        this.liftEncoder = liftEncoder;
    }

    @Override
    protected void initDefaultCommand() {} //Delegated to Robot

    public boolean isAtPosition(LiftPosition position) {
        return liftEncoder.getRaw() < position.getDistance() + LIFT_POSITION_DEADBAND
                && liftEncoder.getRaw() > position.getDistance() - LIFT_POSITION_DEADBAND;
    }

    public void setPosition(LiftPosition position) {
        targetPosition = position;

        if (position == LiftPosition.HOLD_CURRENT) {
            LiftPosition.heldPosition = liftEncoder.getRaw();
        } else {
            LiftPosition.heldPosition = -1;
        }
    }

    //TODO I removed current limiting and upward acceleration.
    // If we need them, I can add them back.

    public void automaticControl() {
        if (liftEncoder.getRaw() < targetPosition.getDistance() + LIFT_POSITION_DEADBAND
                && liftEncoder.getRaw() < LiftPosition.TOP.distance) {
            liftController.set(MAX_UPWARD_COMMAND);
        } else if (liftEncoder.getRaw() > targetPosition.getDistance() - LIFT_POSITION_DEADBAND
                && liftEncoder.getRaw() > LiftPosition.BOTTOM.distance) {
            liftController.set(-MAX_DOWNWARD_COMMAND);
        } else {
            liftController.set(0);
        }
    }

    public void manualControl(double command) {
        liftController.set(command);
    }

    //TODO Should this be refactored into its own file?
    //TODO what if the Commands exposed this, but the Subsystem didn't use it?
    public enum LiftPosition {
        BOTTOM(0),
        SWITCH(58000),
        SCALE(188000),
        TOP(190000),

        /**
         * Used for custom hold-at button
         */
        HOLD_CURRENT(-1);

        private double distance;

        private static double heldPosition; //TODO Relic of the 2018 code, may not be the best way to store this

        LiftPosition(double distance) {
            this.distance = distance;
        }

        public double getDistance() {
            if (this == HOLD_CURRENT) {
                return heldPosition;
            } else {
                return distance;
            }
        }
    }
}
