package com.team980.robot2018neo.commands.lift;

import com.team980.robot2018neo.subsystems.LiftSystem;
import edu.wpi.first.wpilibj.command.Command;

public class HoldLiftAtCurrent extends Command {

    private LiftSystem lift;

    public HoldLiftAtCurrent(LiftSystem lift) {
        this.lift = lift;
    }

    @Override
    protected void execute() {
        lift.automaticControl();
    }

    @Override
    protected boolean isFinished() {
        return false;
    }
}
