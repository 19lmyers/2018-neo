package com.team980.robot2018neo.commands.lift;

import com.team980.robot2018neo.subsystems.LiftSystem;
import edu.wpi.first.wpilibj.command.Command;

public class MoveLiftToPosition extends Command {

    private LiftSystem lift;

    private LiftSystem.LiftPosition targetPosition;

    public MoveLiftToPosition(LiftSystem lift, LiftSystem.LiftPosition targetPosition) {
        this.lift = lift;
        this.targetPosition = targetPosition;
    }

    @Override
    protected void initialize() {
        lift.setPosition(targetPosition);
    }

    @Override
    protected void execute() {
        lift.automaticControl();
    }

    @Override
    protected boolean isFinished() {
        return lift.isAtPosition(targetPosition);
    }
}
