package com.team980.robot2018neo.commands.lift;

import com.team980.powerup.OperatorInterface;
import com.team980.robot2018neo.subsystems.LiftSystem;
import edu.wpi.first.wpilibj.command.Command;

public class ManualLiftControl extends Command {

    private LiftSystem lift;

    private OperatorInterface controls;

    public ManualLiftControl(LiftSystem lift, OperatorInterface controls) {
        this.lift = lift;
        this.controls = controls;
    }

    @Override
    protected void execute() {
        lift.manualControl(controls.getLiftCommand()); //TODO find some way to auto-hold at current if no input?
    }

    @Override
    protected boolean isFinished() {
        return false;
    }
}
