package com.team980.robot2018neo.commands.claw;

import com.team980.robot2018neo.subsystems.ClawSystem;
import edu.wpi.first.wpilibj.command.InstantCommand;

public class ShutClaw extends InstantCommand {

    private ClawSystem claw;

    public ShutClaw(ClawSystem claw) {
        this.claw = claw;

        requires(claw);
    }

    @Override
    protected void initialize() {
        claw.shut();
    }
}
