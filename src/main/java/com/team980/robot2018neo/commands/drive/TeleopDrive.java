package com.team980.robot2018neo.commands.drive;

import com.team980.common.controls.DriverInterface;
import com.team980.common.drive.DifferentialDriveHelper;
import com.team980.robot2018neo.subsystems.DriveSystem;
import edu.wpi.first.wpilibj.command.Command;

public class TeleopDrive extends Command {

    private DifferentialDriveHelper driveHelper;
    private DriverInterface controls;

    public TeleopDrive(DriveSystem drive, DriverInterface controls) {
        this.controls = controls; //TODO we should establish a convention of what order these things should be in

        driveHelper = new DifferentialDriveHelper(drive);

        requires(drive);
    }

    @Override
    protected void execute() {
        driveHelper.arcadeDrive(controls.getThrottle(), controls.getTurn(), true);
    }

    @Override
    protected boolean isFinished() {
        return false;
    }

    @Override
    protected void end() {
       driveHelper.arcadeDrive(0, 0, false); //TODO should I have a stop() method on DriveSystem instead?
    }
}
