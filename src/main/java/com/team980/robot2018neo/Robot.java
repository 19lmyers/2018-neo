package com.team980.robot2018neo;

import com.team980.robot2018neo.commands.drive.TeleopDrive;
import com.team980.robot2018neo.commands.lift.HoldLiftAtCurrent;
import com.team980.robot2018neo.subsystems.ClawSystem;
import com.team980.robot2018neo.subsystems.DriveSystem;
import com.team980.robot2018neo.subsystems.LiftSystem;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.command.Scheduler;

/**
 * Base robot class for FRC Robot programming.
 * Periodic methods are called on a 20ms timer.
 */
public final class Robot extends TimedRobot {

    private Controls controls; //TODO I'm not consistent in ordering Controls before Subsystems / vice versa

    private DriveSystem drive;
    private LiftSystem lift;
    private ClawSystem claw;

    //TODO climb system (when back on robot)

    //TODO sensors
    //TODO tip protect

    //TODO implement auto later

    //TODO diagnostic system (find what templates well)
    // I liked NetworkTables but there are other things (i.e. logs) we could benefit from too

    /**
     * Robot-wide initialization code goes here.
     * Called ONCE when the robot is powered on.
     */
    @Override
    public void robotInit() {
        controls = new Controls();

        var map = new HardwareMap();

        drive = new DriveSystem(map.drive.leftControllers,
                map.drive.leftEncoder,
                map.drive.rightControllers,
                map.drive.rightEncoder,
                map.drive.shifterSolenoid);

        lift = new LiftSystem(map.lift.liftController,
                map.lift.liftEncoder);

        lift.setDefaultCommand(new HoldLiftAtCurrent(lift));

        claw = new ClawSystem(map.claw.clawSolenoid);

        controls.bindButtons(drive, lift, claw);
    }

    /**
     * Runs periodically in all robot modes.
     */
    @Override
    public void robotPeriodic() {
        Scheduler.getInstance().run(); //TODO does this go before or after iterative logic?
    }

    //TODO not sure how to only run things in specific phases in Command based.
    // i.e. TeleopDrive is still running when the robot is disabled!

    /**
     * Called once at the beginning of the autonomous period.
     */
    @Override
    public void autonomousInit() {

    }

    /**
     * Called once at the beginning of the teleoperated period.
     */
    @Override
    public void teleopInit() {
        drive.setDefaultCommand(new TeleopDrive(drive, controls));
    }

    //TODO there should probably be some mechanism to disable (stop) Subsystems? Idk about this

    /**
     * Called once when the robot is disabled.
     * NOTE that this will be called in between the autonomous and teleoperated periods!
     */
    @Override
    public void disabledInit() {
        drive.getDefaultCommand().cancel();
        drive.setDefaultCommand(null);
    }
}