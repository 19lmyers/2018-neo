package com.team980.robot2018neo;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import edu.wpi.first.wpilibj.*;

/**
 * A "map" of the different components of the robot.
 * Should ONLY be accessed by the Robot class!
 */
final class HardwareMap {

    DriveMap drive = new DriveMap();
    LiftMap lift = new LiftMap();
    ClawMap claw = new ClawMap();

    final class DriveMap {

        private static final double WHEEL_RADIUS = 2.0; //in feet
        private static final double ENCODER_PULSES_PER_REVOLUTION = 2048.0;
        private static final double GEAR_RATIO = 1.0;

        SpeedControllerGroup leftControllers;
        SpeedControllerGroup rightControllers;

        Encoder leftEncoder;
        Encoder rightEncoder;

        Solenoid shifterSolenoid;

        private DriveMap() {
            var leftTopMotor = new WPI_TalonSRX(0);
            leftTopMotor.setInverted(true);
            var leftFrontMotor = new WPI_TalonSRX(1);
            var leftBackMotor = new WPI_TalonSRX(2);

            leftControllers = new SpeedControllerGroup(leftTopMotor, leftFrontMotor, leftBackMotor);
            leftControllers.setName("Drive System", "Left Speed Controllers");

            var rightTopMotor = new WPI_TalonSRX(3);
            rightTopMotor.setInverted(true);
            var rightFrontMotor = new WPI_TalonSRX(4);
            var rightBackMotor = new WPI_TalonSRX(5);

            rightControllers = new SpeedControllerGroup(rightTopMotor, rightFrontMotor, rightBackMotor);
            rightControllers.setName("Drive System", "Right Speed Controllers");

            leftEncoder = new Encoder(2, 3, false, CounterBase.EncodingType.k4X);
            leftEncoder.setDistancePerPulse((2 * Math.PI * (WHEEL_RADIUS / 12)) / ENCODER_PULSES_PER_REVOLUTION * GEAR_RATIO);
            leftEncoder.setPIDSourceType(PIDSourceType.kRate);
            leftEncoder.setName("Drive System", "Left Encoder");

            rightEncoder = new Encoder(4, 5, true, CounterBase.EncodingType.k4X);
            rightEncoder.setDistancePerPulse((2 * Math.PI * (WHEEL_RADIUS / 12)) / ENCODER_PULSES_PER_REVOLUTION * GEAR_RATIO);
            rightEncoder.setPIDSourceType(PIDSourceType.kRate);
            rightEncoder.setName("Drive System", "Right Encoder");

            shifterSolenoid = new Solenoid(0);
            shifterSolenoid.setName("Drive System", "Shifter Solenoid");
        }
    }

    final class LiftMap {

        WPI_TalonSRX liftController;

        Encoder liftEncoder;

        private LiftMap() {
            liftController = new WPI_TalonSRX(11);
            liftController.setName("Lift System", "Lift Speed Controller");

            liftEncoder = new Encoder(1, 2, false, CounterBase.EncodingType.k4X);
            liftEncoder.setName("Lift System", "Lift Encoder");
        }
    }

    final class ClawMap {

        Solenoid clawSolenoid;

        private ClawMap() {
            clawSolenoid = new Solenoid(1);
            clawSolenoid.setName("Claw System", "Claw Solenoid");
        }
    }
}
