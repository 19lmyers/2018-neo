package com.team980.common.controls;

/**
 * A robot-agnostic interface for our standard driver controls.
 */
public interface DriverInterface {
    double getThrottle();
    double getTurn();
}
