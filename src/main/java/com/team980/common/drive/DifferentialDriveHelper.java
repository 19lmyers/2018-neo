package com.team980.common.drive;

//TODO How do we want to structure this class?
// Does it consume DriverInterface? DriveSystem?
// Do we use Pair? Getters? Arrays?
// Should we support squaredInputs? tankDrive?
// Should it be coupled to the code? In common?
// Do we need interface(s) for consumed classes to implement?
// Is the priority matching the WPI implementation? Cleaning it up?
public class DifferentialDriveHelper {

    private DifferentialDrive drive;

    public DifferentialDriveHelper(DifferentialDrive drive) {
        this.drive = drive;
    }

    /**
     * @param move         Requested move command, from -1 to 1
     * @param turn         Requested turn command, from -1 to 1
     * @param squareInputs If set, decreases the input sensitivity at low speeds.
     *
     * @see edu.wpi.first.wpilibj.drive.DifferentialDrive#arcadeDrive(double, double, boolean)
     */
    public void arcadeDrive(double move, double turn, boolean squareInputs) {
        if (squareInputs) {
            move = Math.copySign(move * move, move);
            turn = Math.copySign(turn * turn, turn);
        }

        double left;
        double right;

        var maxInput = Math.copySign(Math.max(Math.abs(move), Math.abs(turn)), move);

        if (move >= 0.0) {
            // First quadrant, else second quadrant
            if (turn >= 0.0) {
                left = maxInput;
                right = move - turn;
            } else {
                left = move + turn;
                right = maxInput;
            }
        } else {
            // Third quadrant, else fourth quadrant
            if (turn >= 0.0) {
                left = move + turn;
                right = maxInput;
            } else {
                left = maxInput;
                right = move - turn;
            }
        }

        drive.set(left, right);
    }

    public interface DifferentialDrive {
        void set(double left, double right);
    }
}
