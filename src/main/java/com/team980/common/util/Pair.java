package com.team980.common.util;

import java.util.Objects;

@Deprecated //TODO I think we can avoid this
public class Pair<A, B> {

    public final A left;
    public final B right;

    public Pair(A left, B right) {
        this.left = left;
        this.right = right;
    }

    public static <A, B> Pair<A, B> of(A left, B right) {
        return new Pair<>(left, right);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Pair)) return false;

        Pair<?, ?> p = (Pair<?, ?>) o;
        return Objects.equals(left, p.left) && Objects.equals(right, p.right);
    }

    @Override
    public int hashCode() {
        return Objects.hash(left, right);
    }
}
