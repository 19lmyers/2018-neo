package com.team980.common.util;

import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.SpeedController;

@Deprecated //TODO do we want this? I don't think so
public class SensorTransmission {
    SpeedController controller;
    Encoder encoder;

    public SensorTransmission(SpeedController controller, Encoder encoder) {
        this.controller = controller;
        this.encoder = encoder;
    }
}
